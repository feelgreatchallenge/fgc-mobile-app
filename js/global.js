function actuateLink(link) {
   var allowDefaultAction = true;
      
   if (link.click)
   {
      link.click();
      return;
   }
   else if (document.createEvent)
   {
      var e = document.createEvent('MouseEvents');
      e.initEvent(
	 'click'     // event type
	 ,true      // can bubble?
	 ,true      // cancelable?
      );
      allowDefaultAction = link.dispatchEvent(e);           
   }
	 
   if (allowDefaultAction)       
   {
      var f = document.createElement('form');
      f.action = link.href;
      document.body.appendChild(f);
      f.submit();
   }
}
$(function(event) {
	var ref = window.open('https://my.feelgreatchallenge.com.au/', '_blank', 'location=no');
	window.location = 'https://my.feelgreatchallenge.com.au/';
	actuateLink('start-link');
});
